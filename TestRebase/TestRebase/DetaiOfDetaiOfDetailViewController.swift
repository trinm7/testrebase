//
//  DetaiOfDetaiOfDetailViewController.swift
//  TestRebase
//
//  Created by Duc Anh Tran on 14/12/2021.
//

import UIKit

class DetaiOfDetaiOfDetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickToPushVC(_ sender: Any) {
        if let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
